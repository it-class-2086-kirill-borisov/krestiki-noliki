a = [[7, 8, 9], [4, 5, 6], [1, 2, 3]]

# Для игры необходимо поочередно вводить в консоль координаты нужной ячейки на игровом поле. Первой координатой является
# номер строки, второй - номер столбца. Нумерация ведётся с единицы

def main():
    def hello():
        return 'приятной игры!'

    def uppercase_decorator(function):
        def wrapper():
            func = function()
            make_uppercase = func.upper()
            return make_uppercase

        return wrapper

    @uppercase_decorator
    def hello():
        return 'приятной игры!'

    print(hello())

    # Функция, сканирующаяя всё поле на наличие трёх в ряд. Ничего умнее пока что не придумал...
    
    def check_victory(a, x, y):
        if (a[0][0] == a[0][1] == a[0][2]) or (a[1][0] == a[1][1] == a[1][2]) or (a[2][0] == a[2][1] == a[2][2]) or (
                a[0][0] == a[1][0] == a[2][0]) or (a[0][1] == a[1][1] == a[2][1]) or (
                a[0][2] == a[1][2] == a[2][2]) or (a[0][0] == a[1][1] == a[2][2]) or (a[0][2] == a[1][1] == a[2][0]):
            print("WIN")
            exit(0)

    # Функция, выводящая в консоль игровое поле
    def output(a):
        for i in range(3):
            for j in range(3):
                print(a[i][j], end='  ')
            print()


    count = 3
    steps = 0
    output(a)
    while True:
        x = int(input())
        y = int(input())
        if count % 2 != 0:
            a[x - 1][y - 1] = "x"
        else:
            a[x - 1][y - 1] = "o"
        count = count + 1
        steps = steps + 1
        output(a)
        if steps == 9:
            print("Frienship won!")
            exit(0)
        check_victory(a, x, y)

main()
